class RPNCalculator
  # TODO: your code goes here!
  attr_accessor :stack

  def initialize
    @stack = []
  end

  def push(int)
    @stack.push(int.to_f)
  end

  def popValue
    value = @stack.pop
    if value == nil
      raise 'calculator is empty'
    else
      value
    end
  end

  def value
    @stack.last
  end

  def plus
    @stack.push(popValue + popValue)
  end

  def minus
    @stack.push(-popValue + popValue)
  end

  def times
    @stack.push(popValue * popValue)
  end

  def divide
    divisor = popValue
    dividend = popValue
    @stack.push(dividend / divisor)
  end

  def tokens(string)
    ops = {'+' => :+, '-' => :-, '*' => :*, '/' => :/}
    string.split.map do |seg|
      if ops[seg] != nil
         ops[seg]
      else
        seg.to_f
      end
    end
  end

  def evaluate(string)
    ops = {'+' => Proc.new{plus}, '-' => Proc.new{minus}, '*' => Proc.new{times}, '/' => Proc.new{divide}}
    @stack = []
    string.split.each do |seg|
      #puts seg, @stack
      if ops[seg] != nil
         ops[seg].call
      else
        @stack.push(seg.to_f)
      end
    end
    value
  end

end
